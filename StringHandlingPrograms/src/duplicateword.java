import java.io.*;

public class duplicateword 
{

	public static void main(String[] args) 
	{
	
	        String string = "Big black bug bit a big black dog on his big black nose";  
	        int count;  
	          
	        //Converts the string into lowercase  
	        string = string.toLowerCase();  
	          
	        //Split the string into words using built-in function  
	        String words[] = string.split(" ");  
	          
	        System.out.println("Duplicate words in a given string : ");   
	        for(int i = 0; i < words.length; i++) {  
	            count = 1;  
	            for(int j = i+1; j < words.length; j++) {  
	                if(words[i].equals(words[j])) {  
	                    count++;  
	                    //Set words[j] to 0 to avoid printing visited word  
	                    words[j] = "0";  
	                }  
	            }  
	              
	            //Displays the duplicate word if count is greater than 1  
	            if(count > 1 && words[i] != "0")  
	                System.out.println(words[i]);  
	        }  
	    }  
	
/*
 *  In this program, we need to find out the duplicate words present in the string and display those words.

 *   To find the duplicate words from the string, we first split the string into words. 
 *   We count the occurrence of each word in the string. 
 *   If count is greater than 1, it implies that a word has duplicate in the string.

Algorithm
Define a string.
Convert the string into lowercase to make the comparison insensitive.
Split the string into words.
Two loops will be used to find duplicate words. Outer loop will select a word and Initialize variable count to 1. Inner loop will compare the word selected by outer loop with rest of the words.
If a match found, then increment the count by 1 and set the duplicates of word to '0' to avoid counting it again.
After the inner loop, if count of a word is greater than 1 which signifies that the word has duplicates in the string.
*
*
*  	
*/	
}
