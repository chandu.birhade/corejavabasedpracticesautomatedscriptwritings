import java.io.*;
public class Frequency 
{

	public static void main(String[] args) {  
        String str = "picture perfect";  
        int[] freq = new int[str.length()];  
        int i, j;  
          
        //Converts given string into character array  
        char string[] = str.toCharArray();  
          
        for(i = 0; i <str.length(); i++) {  
            freq[i] = 1;  
            for(j = i+1; j <str.length(); j++) {  
                if(string[i] == string[j]) {  
                    freq[i]++;  
                      
                    //Set string[j] to 0 to avoid printing visited character  
                    string[j] = '0';  
                }  
            }  
        }  
          
        //Displays the each character and their corresponding frequency  
        System.out.println("Characters and their corresponding frequencies");  
        for(i = 0; i <freq.length; i++) {  
            if(string[i] != ' ' && string[i] != '0')  
                System.out.println(string[i] + "-" + freq[i]);  
        }  
    }  

/*  Algorithm
	
	a. Define a string.
	b. Define an array freq with the same size of the string.
	c. Two loops will be used to count the frequency of each character. 
	   Outer loop will be used to select a character and initialize element at corresponding index in array freq with 1.
	d. Inner loop will compare the selected character with rest of the characters present in the string.
	e. If a match found, increment element in freq by 1 and set the duplicates of selected character by '0' to mark them as visited.
	f. Finally, display the character and their corresponding frequencies by iterating through the array freq
	
	
	In this program, we need to find the frequency of each character present in the word.

   To accomplish this task, we will maintain an array called freq with same size of the length of the string. 
   Freq will be used to maintain the count of each character present in the string. 
   Now, iterate through the string to compare each character with rest of the string. 
   Increment the count of corresponding element in freq. Finally, iterate through freq to display the frequencies of characters.


	
	chandrakant birhade
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	*/
}
